<html>
    <head>
        <title>Electron Test Task</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1" />
        <link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
        <link href="html/css/header.css" rel="stylesheet" type="text/css">
        <link href="html/css/main.css" rel="stylesheet" type="text/css">
        <script async src="/html/js/main.js"></script>
    </head>
    <body>
        <header>
            <div class="content">
                <div class="company">
                    <img class="logo" src="html/img/logo.png">
                    <input class="search" type="text" placeholder="Поиск">
                </div>
                <div class="user" id="user-preview">
                    <p>Электрон</p>
                    <img class="avatar-small" src="html/img/avatar.png">
                    <div class="arrow"></div>
                    <div class="top-menu" id="top-menu">
                        <ul>
                            <li><a href="#">Моя страница</a></li>
                            <li><a href="#">Выйти</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
        <main>
            <div class="content">
                <div class="profile">
                    <div class="profile-data">
                        <h1>Электрон Вебов</h1>
                        <span id="current-status">изменить статус</span>
                        <form class="status-form" id="status-change-form">
                            <input id="status-input" class="status-input" type="text" placeholder="Изменить статус">
                            <button id="status-confirm" class="status-confirm">Сохранить</button>
                        </form>
                        <hr noshade color="#e8e8e8" size="1">
                        <table>
                            <tr>
                                <td>День рождения:</td>
                                <td><a href="#">23 сентября 1988 г.</a></td>
                            </tr>
                            <tr>
                                <td>Семейное положение:</td>
                                <td><a href="#">женат</a></td>
                            </tr>
                            <tr>
                                <td>Образование:</td>
                                <td><a href="#">ИжГТУ им. М.Т.Калашникова (бывш.ИМИ)'12</a></td>
                            </tr>
                            <tr>
                                <td>Веб-сайт:</td>
                                <td><a href="www.web-electron.ru">www.web-electron.ru</a></td>
                            </tr>
                        </table>
                        <div class="flex">
                        <h4>Немного о себе:</h4>
                        <div class="line"></div>
                        </div>
                        <p>С другой стороны новая модель организационной деятельности влечет за собой процесс внедрения и модернизации новых предложений. Товарищи! новая модель организационной деятельности в значительной степени обуславливает создание новых предложений.</p>
                        <p>Не следует, однако забывать, что укрепление и развитие структуры в значительной степени обуславливает создание систем массового участия.</p>
                        <p>Идейные соображения высшего порядка, а также постоянное информационно-пропагандистское обеспечение нашей деятельности требуют от нас анализа направлений прогрессивного развития.</p>
                        <p>Задача организации, в особенности же укрепление и развитие структуры позволяет оценить значение направлений прогрессивного развития.</p>
                    </div>
                    <div class="profile-photo">
                        <img src="/html/img/avatar.png">
                    </div>
                </div>
                <div class="gallery">
                    <h4>Фотографии Электрона</h4>
                    <div class="gallery-photos">
                        <img src="/html/img/gallery/photo_1.jpg">
                        <img src="/html/img/gallery/photo_2.jpg">
                        <img src="/html/img/gallery/photo_3.jpg">
                        <img src="/html/img/gallery/photo_4.jpg">
                    </div>
                </div>
                <div class="wall">
                    <div id="comments" class="comments">
                        <h3>Записи на вашей стене</h3>
                    </div>
                    <form id="comment-form">
                        <h3>Добавить запись</h3>
                        <input class="comment-form-name" type="text" placeholder="Ваше имя">
                        <textarea class="comment-form-text" placeholder="Текст записи..."></textarea>
                        <button class="comment-form-submit">Отправить</button>
                    </form>
                </div>
            </div>
        </main>
        <footer>
            <div class="content"><span>elbook 2018</span></div>
        </footer>
    </body>
</html>