'use strict'

const month = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];

let topMenu = document.getElementById('top-menu');
let userPreview = document.getElementById('user-preview');
let currentStatus = document.getElementById('current-status');
let statusForm = document.getElementById('status-change-form');
let statusInput = document.getElementById('status-input');
let commentsContainer = document.getElementById('comments');
let commentForm = document.getElementById('comment-form');

userPreview.onclick = toggleMenu;
currentStatus.onclick = showStatusForm;
statusForm.onsubmit = saveStatus;
commentForm.onsubmit = addComment;
setStatus();
initPosts();

function toggleMenu() {
    let visible = topMenu.style.display === 'block';

    if (visible) {
        topMenu.style.display = 'none';
    } else {
        topMenu.style.display = 'block';
    }
}

function showStatusForm() {
    statusForm.style.display = 'block';
}

function hideStatusForm() {
    statusForm.style.display = 'none';
}

function saveStatus(e) {
    e.preventDefault();
    localStorage.setItem('status', statusInput.value);
    setStatus();
    hideStatusForm();
}

function setStatus() {
    let statusValue = localStorage.getItem('status');

    if (statusValue) {
        statusInput.value = statusValue;
        currentStatus.innerText = statusValue;
    } else {
        currentStatus.innerText = 'Изменить статус';
    }
}

function getPosts() {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'html/php/ajax.php?action=getAll', false);
    xhr.send();

    return JSON.parse(xhr.responseText);
}

function initPosts() {
    let posts = getPosts();

    posts.forEach(function (post) {
        addPost(post);
    })

}

function addPost(post) {
    let avatar = post.avatar ? post.avatar : 'html/img/avatars/default.png'
    let datetime = getDateTime(post.datetime);
    let element = document.createElement('div');
    element.className = 'post';
    element.id = `post-${post.id}`;
    element.innerHTML = `<div class="post-card">
                            <img class="post-avatar" src="${avatar}">
                            <div class="post-info">
                                <a class="post-name" href="#">${post.name}</a>
                                <span class="post-datetime">${datetime}</span>
                            </div>
                        </div>
                        <p class="post-content">
                            ${post.text}
                        </p>
                        <span onclick="deletePost(${post.id})" class="close"></span>`
    commentsContainer.append(element);
}

function deletePost(id) {
    let xhr = new XMLHttpRequest();
    xhr.open('POST', `html/php/ajax.php?action=delete`, false);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send(`id=${id}`);

    let post = document.getElementById(`post-${id}`);
    post.remove();
}

function addComment(e) {
    e.preventDefault();
    let name = commentForm.querySelector('input').value;
    let text = commentForm.querySelector('textarea').value;

    let xhr = new XMLHttpRequest();
    xhr.open('POST', `html/php/ajax.php?action=add`, false);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send(`name=${name}&text=${text}`);

    let id = xhr.responseText;

    addPost({id, name, text});
}

function getDateTime(timestamp) {
    let currentDate = new Date();
    let date;

    if (timestamp) {
        date = new Date(timestamp * 1000);
    } else {
        return `только что`;
    }

    let diff = (Number(currentDate) - Number(date)) / 1000;

    if (diff < 60) {
        return `${Math.round(diff)} секунд назад`;
    }

    if (diff < 60 * 60) {
        return `${Math.round(diff / 60)} минут назад`;
    }

    if (
        currentDate.getFullYear() === date.getFullYear() &&
        currentDate.getMonth() === date.getMonth() &&
        currentDate.getDate() === date.getDate()
    ) {
        return `сегодня в ${date.getHours()}:${addZero(date.getMinutes())}`;
    }

    if (
        currentDate.getFullYear() === date.getFullYear() &&
        currentDate.getMonth() === date.getMonth() &&
        currentDate.getDate() - date.getDate() === 1
    ) {
        return `вчера в ${date.getHours()}:${addZero(date.getMinutes())}`;
    }

    return `${date.getDate()} ${month[date.getMonth()]} ${date.getFullYear()}`;
}

function addZero(number) {
    if (number < 10) {
        number = "0" + number;
    }
    return number;
}