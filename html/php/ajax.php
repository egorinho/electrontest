<?php

include 'DbService.php';
if (!isset($_GET['action'])) {
    return;
}

$dbService = new DbService();

switch ($_GET['action']) {
    case 'getAll':
        $posts = $dbService->findAllPosts();
        echo json_encode($posts);
        break;
    case 'delete':
        $dbService->deletePost($_POST['id']);
        break;
    case 'add':
        $dbService->insertPost($_POST['name'], $_POST['text'], time());
        echo json_encode($dbService->db->lastInsertRowID());
        break;
    default:
        return;
}




