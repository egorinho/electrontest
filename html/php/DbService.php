<?php

class DbService
{
    private const DB_PATH = '../../db/mydb.db';

    /** @var SQLite3 */
    public $db;

    public function __construct()
    {
        if (file_exists(self::DB_PATH)) {
            $this->openDb();
        } else {
            $this->initDb();
        }
    }

    public function insertPost(string $name, string $text, int $datetime, ?string $avatar = null): void
    {
        $sql = "INSERT INTO post (name, text, datetime, avatar) VALUES ('$name', '$text', '$datetime', '$avatar')";
        $this->db->query($sql);
    }

    public function deletePost(int $id): void
    {
        $sql = "DELETE FROM post WHERE id=$id";
        $this->db->query($sql);
    }

    public function findAllPosts(): array
    {
        $result = $this->db->query('SELECT * FROM post');
        $arr = [];

        while ($data = $result->fetchArray(SQLITE3_ASSOC)) {
            $arr[] = $data;
        }

        return $arr;
    }

    private function initDb(): void
    {
        $this->openDb();
        $this->createTable('post', ['id INTEGER PRIMARY KEY', 'name TEXT', 'text TEXT', 'datetime integer', 'avatar TEXT']);
        $this->insertPost(
            'Дэдпул Батькович',
            'Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько
                                абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору
                                отточить навык публичных выступлений в домашних условиях. При создании генератора мы
                                использовали небезизвестный универсальный код речей. Текст генерируется абзацами
                                случайным образом от двух до десяти предложений в абзаце, что позволяет сделать
                                текст более привлекательным и живым для визуально-слухового восприятия.
                                По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который
                                вызывает у некторых людей недоумение при попытках прочитать рыбу текст. В отличии от
                                lorem ipsum, текст рыба на русском языке наполнит любой макет непонятным смыслом и
                                придаст неповторимый колорит советских времен.',
            1598371560,
            '/html/img/avatars/deadpool.png'
        );
       $this->insertPost(
           'Россомаха Логинович',
           'Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько
                                абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору
                                отточить навык публичных выступлений в домашних условиях. При создании генератора мы
                                использовали небезизвестный универсальный код речей. Текст генерируется абзацами
                                случайным образом от двух до десяти предложений в абзаце, что позволяет сделать
                                текст более привлекательным и живым для визуально-слухового восприятия.
                                По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который
                                вызывает у некторых людей недоумение при попытках прочитать рыбу текст. В отличии от
                                lorem ipsum, текст рыба на русском языке наполнит любой макет непонятным смыслом и
                                придаст неповторимый колорит советских времен.',
           1598371560,
           '/html/img/avatars/logan.png'
       );
    }

    private function openDb(): void
    {
        $this->db = new SQLite3(self::DB_PATH);
    }

    private function createTable(string $tableName, array $columns): void
    {
        $columnsConfig = implode(',', $columns);
        $sql = "CREATE TABLE $tableName($columnsConfig)";

        $this->db->query($sql);
    }
}
